#!/bin/bash

root="$PWD"
infiles="$root/filenames.txt"
outfile="$root/results.txt"

mem="1G"
jarfile="$root/target/seg-1.0-SNAPSHOT-jar-with-dependencies.jar"
CP="$jarfile"

java -Xmx$mem -cp $CP edu.umass.cs.iesl.seg.Heuristic $infiles $outfile
