package edu.umass.cs.iesl.seg

import scala.io.Source
import scala.collection.mutable.ArrayBuffer
import org.apache.spark.SparkContext
import org.apache.spark.SparkContext._
import org.apache.spark.SparkConf
import Heuristic.extract
import java.io.{PrintWriter, File}


/**
 * Created by kate on 2/18/16.
 */
object Segmenter {

  def main(args: Array[String]): Unit = {
    val conf = new SparkConf().setAppName("emo")
    val sc = new SparkContext(conf)
    println(args.mkString(" "))
    val infile = args(0)
    val outfile = args(1)
    val filenames = Source.fromFile(infile).getLines().toSeq
    filenames.foreach(println)
    val scfile = sc.textFile(filenames.mkString(","))
    val counts = scfile.flatMap(line => extract(line))
    val agg = counts.reduceByKey { case (a, b) => a + b }.sortBy { case (k, v) => v }
    val ntypes = agg.count()
    val output = agg.collect().reverse
    val total = agg.values.reduce { case (a, b) => a + b }
    val pw = new PrintWriter(new File(outfile), "UTF8")
    pw.write(s"total emos: $total\n")
    pw.write(s"total types: $ntypes\n")
    pw.write(s"total lines: ${scfile.count()}\n")
    pw.write(s"total files: ${filenames.length}\n")
    output.foreach { case (w, c) => pw.write(s"$w $c\n") }
    pw.close()
  }

}
