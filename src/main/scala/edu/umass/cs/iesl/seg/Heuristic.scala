package edu.umass.cs.iesl.seg

import scala.collection.mutable.ArrayBuffer
import scala.io.Source
import scala.util.matching.Regex

/**
 * Created by kate on 2/18/16.
 */
object Heuristic {

  val patterns: Map[String, Regex] = Map(
    "wleft" -> "([（()）]+[~-]?(\\s)*[~-]?[=:;；]+)".r,
    "wright" -> "([=:;；]+[~-]?(\\s)*[~-]?[)）]+)".r,
    "eastern2" -> "(\\W{1,2}[（(]+\\s*\\W{2,10}\\s*[)）]\\W{1,2})".r,
    "eastern" -> "([（(]+\\s*\\W{2,10}\\s*[（()）]+)".r,
    "eastern_asym" -> "([（(]+\\s*\\W{2,10}\\s*[)）]+\\W{1,4})".r,
    "eastern_asym2" -> "(\\W{1,4}[（(]+\\s*\\W{2,10}\\s*[)）]+)".r,
    "hearts" -> "(<+3+)|([❤♡]+)".r
  )

  def main(args: Array[String]): Unit = {
    println(args.mkString(" "))
    val infile = args(0)
    val outfile = args(1)
    val filenames = Source.fromFile(infile).getLines().toSeq
    filenames.foreach(println)
    val check: Seq[String] = load(filenames.head)
    for (txt <- check) {
      val results = extract(txt)
      if (results.nonEmpty) {
        println(txt)
        results.foreach { case (emo, ct) => println(s"$emo $ct") }
      }
    }
  }

  def extract(text: String): Seq[(String, Int)] = {
    var emo: String = ""
    var count = 0
    val results = new ArrayBuffer[(String,Int)]()
    var found = false
    val pseq = patterns.toSeq
    var i = 0
    while (!found && i < pseq.length) {
      val p = pseq(i)
      val id = p._1
      val regex = p._2
      val matches = regex.findAllIn(text)
      if (matches.nonEmpty) {
        for (m: String <- matches) {
          val pair: (String, Int) = (m, 1)
          results += pair
        }
        found = true
      } else {
        i += 1
      }
    }
    results.toSeq
  }


  def load(filename: String): Seq[String] = {
    val buff = new ArrayBuffer[String]()
    val lines = Source.fromFile(filename).getLines()
    while (lines.hasNext) {
      val line = lines.next()
      val n = line.length
      buff += line.trim.drop(1).dropRight(1)
    }
    buff.toSeq
  }


}
