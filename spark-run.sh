#!/bin/bash

root="$PWD"
jarfile="$root/seg-1.0-SNAPSHOT.jar"
master="spark://10.200.1.1:7077"

spark-submit \
--class "edu.umass.cs.iesl.seg.Segmenter" \
--master $master \
$jarfile \
$root/filenames \
$root/2016-02-18-results.txt
